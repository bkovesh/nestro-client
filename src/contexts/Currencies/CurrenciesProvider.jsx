import {
  createContext,
  useCallback,
  useContext, useEffect, useState
} from 'react'

import { Currencies as CurrenciesService } from '../../api/Currencies'
import { MAIN_CURRENCIES } from '../../constants/currencies'
import { arrayToObject } from '../../helpers/objects'

const Currencies = new CurrenciesService()

const CurrenciesContext = createContext({
  getCurrencyByDate: () => {}
})

export const useCurrencies = () => useContext(CurrenciesContext)

const CurrenciesProvider = (props) => {
  const { children } = props

  const getCurrencyByDate = useCallback(async (props) => {
    try {
      console.log('@CurrenciesProvider > getCurrencyByDate', { props })
      const { date, currency } = props
      const result = await Currencies.getCurrenciesByDate({
        date
      })
      console.log('@CurrenciesProvider > getCurrencyByDate', { result })
      return Object.entries(result?.Valute).filter(([key]) => currency === key)
    } catch (error) {
      console.error(error)
      return null
    }
  }, [])

  const [currencies, setCurrencies] = useState([])
  const [currenciesBySymbol, setCurrenciesBySymbol] = useState({})

  const getCurrencies = useCallback(async (date) => {
    try {
      const result = await Currencies.getCurrenciesByDate({
        date
      })
      const resultFiltered = Object.entries(result?.Valute)?.filter?.(([key]) => MAIN_CURRENCIES.includes(key))
      setCurrencies(resultFiltered)
      setCurrenciesBySymbol(arrayToObject(resultFiltered))
    } catch (error) {
      console.error(error)
    }
  }, [])

  return (
    <CurrenciesContext.Provider
      value={{
        getCurrencyByDate,
        getCurrencies,
        currenciesBySymbol,
        currencies
      }}
    >
      {children}
    </CurrenciesContext.Provider>
  )
}

export default CurrenciesProvider
