import { useCallback, useMemo } from 'react'
import { format } from "date-fns"

export const useCalculateInvoices = (props) => {
  const { invoices } = props

  const formatTime = useCallback((item) => {
    try {
      if (item) {
        const date = new Date(item)
        const offset = date.getTimezoneOffset()
        const timestamp = date.getTime() - +offset * 60 * 1000
        const result = format(new Date(timestamp), 'dd.MMM.yyyy')
        return result
      }
      return item
    } catch (error) {
      console.error(error)
      return item
    }
  }, [])

  const invoicesCalculated = useMemo(() => {
    const data = invoices?.data?.map?.((item, i) => {
      item.G = +item.B + +item.C + +item.D + +item.E + +item.F

      item.M = formatTime(item.M)
      item.N = formatTime(item.N)
      item.P = formatTime(item.P)

      return item
    })
    return data
  },[invoices, formatTime])

  return invoicesCalculated
}
