import { useMemo } from 'react'

import s from './table.module.scss'

const Table = (props) => {
  const { data, headers } = props ?? {}

  const headersFromData = useMemo(() => Object.keys(data?.[0] ?? {}), [data])

  return (
    <div className={s.root}>
        {headers ?
          headers?.map?.((item, i) => {
            const values = Object.entries(item)
            return (
              <div className={s.header} key={`table-header-${i}`}>
                {values?.map?.(([key, value], i2) => {
                  return (<div key={`table-header-item-${i2}`} className={s.item}>{key}: {value}</div>)
                })}
              </div>
            )
          }) :
          <div className={s.header}>
            {headersFromData?.map?.((item, i) => {
              return (<div key={`table-header2-item-${i}`} className={s.item}>{item}</div>)
            })}
          </div>
        }
      <div className={s.rows}>
        {data?.map?.((item, i) => {
          if (!headers && !i) return
          return (
            <div key={`table-row-${i}`} className={s.row}>
              {Object.values(item ?? {})?.map?.((item2, i2) => {
                if (typeof item2 === 'number') item2 = item2.toFixed(2)
                return (<div key={`table-row-item-${i2}`} className={s.item}>{item2}</div>)
              })}
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default Table
