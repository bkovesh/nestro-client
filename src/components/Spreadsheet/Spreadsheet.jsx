import { useCallback, useEffect, useState } from 'react'
import XSpreadsheet from "x-data-spreadsheet";
import * as XLSX from 'xlsx'

import s from './spreadsheet.module.scss'

import { stox, xtos } from '../../helpers/excel'

const OPTIONS = {
  mode: 'edit', // edit | read
  showToolbar: true,
  showGrid: true,
  showContextmenu: true,
  view: {
    height: () => document.documentElement.clientHeight - 100,
    width: () => document.documentElement.clientWidth - 100,
  },
  row: {
    len: 100,
    height: 25,
  },
  col: {
    len: 100,
    width: 100,
    indexWidth: 60,
    minWidth: 60,
  },
  style: {
    bgcolor: '#ffffff',
    align: 'left',
    valign: 'middle',
    textwrap: false,
    strike: false,
    underline: false,
    color: '#0a0a0a',
    font: {
      name: 'Helvetica',
      size: 10,
      bold: false,
      italic: false,
    },
  },
}

const Spreadsheet = (props) => {
  const { data } = props ?? {}

  const [table, setTable] = useState()

  const load = useCallback(() => {
    if (!data) return
    const tableNew = new XSpreadsheet("#spreadsheet", {})
      .loadData(stox(data))
    setTable(tableNew)
  }, [data])

  useEffect(() => {
    const element = document.getElementById('spreadsheet')
    if (element?.children?.length > 1) return
    load()
  }, [data])

  const handleExportXlsx = useCallback(() => {
    XLSX.writeFile(xtos(table.getData()), "Экспорт.xlsx");
  }, [table])

  return (
    <div id='spreadsheet' className={s.root}>
      <div className={s.buttons}>
        <button className={s.button} onClick={handleExportXlsx}>Экспорт</button>
      </div>
    </div>
  )
}

export default Spreadsheet
