import React, { Fragment } from 'react'
import { Canvas } from '@react-three/fiber'
import Box from '../../3D/Box/Box'
import {
  CameraControls,
} from '@react-three/drei'

import s from './invoices-bar.module.scss'

import useBar from './useBar'
import Axis from '../../3D/Axis/Axis'
import Lights from '../../3D/Lights/Lights'
import { toFixed } from '../../../helpers/numbers'

function InvoicesBar(props) {
  const { data = {}, axis, selectedColumn } = props

  const {
    minMaxValues,
    DISTANCE_BETWEEN_BOXES,
    MAX_HEIGHT,
    axisLengths
  } = useBar({ data: data.data, selectedColumn })

  return (
    <div className={s.root}>
      <Canvas shadows colorManagement camera={{ fov: 45, position: [15, 15, 15] }} gl={{ antialias: true }}>
        {/* https://github.com/yomotsu/camera-controls */}
        <CameraControls minDistance={5} smoothTime={0.01} />

        <Lights />

        <group>
          <Axis axis={axis} axisLengths={axisLengths} />

          {data?.data?.map?.((item, i) => {
            try {
              const entries = Object.entries(item)
              return (
                <Fragment key={`bar-item-${i}`}>
                  {entries?.map(([key, value], i2) => {
                    if (key !== selectedColumn) return null
                    const height = MAX_HEIGHT * value / (minMaxValues?.maxValue ?? 1)
                    const position = [
                      0,
                      height / 2,
                      0.5 + (0.5 + DISTANCE_BETWEEN_BOXES) * i
                    ]
                    const header = data?.headers?.[1]?.[key] ?? ''
                    return (
                      <Fragment key={`bar-item-${i}-${i2}`}>
                        <Box
                          hasEdges
                          dimensions={[0.4, height, 0.4]}
                          position={position}
                          color={'#ff9800'}
                          hoverColor={'gray'}
                          // transparent
                          // opacity={0.5}
                          label={
                            <div className={s.label}>
                              {!!header && <p className={s.brand}>{header}</p>}
                              <p className={s.value}>{toFixed(value, 2)}</p>
                            </div>
                          }
                        />
                      </Fragment>
                    )
                  })}
                </Fragment>
              )
            } catch (error) {
              return null
            }
          })}
        </group>
      </Canvas>
    </div>
  )
}

export default InvoicesBar
