import React, { useMemo } from 'react'

const DISTANCE_BETWEEN_BOXES = 0.01
const MAX_HEIGHT = 3

const useBar = (props) => {
  const { data, selectedColumn } = props

  const minMaxValues = useMemo(() => {
    let rowsCount = data?.length
    let columnsCount = 0
    let values = []
    data?.forEach?.((item) => {
      const entries = Object.entries(item)
      if (entries.length > columnsCount) columnsCount = entries.length
      entries?.forEach(([key, value], i) => {
        if (!!selectedColumn && key !== selectedColumn) return
        values.push(value)
      })
    })
    const minValue = values?.sort((a, b) => a - b)?.[0]
    const maxValue = values?.sort((a, b) => b - a)?.[0]
    const difference = maxValue - minValue
    return { minValue, maxValue, rowsCount, columnsCount, difference }
  }, [data, selectedColumn])

  const axisLengths = useMemo(() => ({
    x: (minMaxValues.columnsCount * 0.4 + 1) * 1.3,
    y: MAX_HEIGHT * 1.3,
    z: (minMaxValues.rowsCount * 0.4 + 1) * 1.3
  }), [MAX_HEIGHT, minMaxValues])

  return { minMaxValues, DISTANCE_BETWEEN_BOXES, MAX_HEIGHT, axisLengths }
}

export default useBar
