import React, { Fragment, useMemo, useRef } from 'react'
import { Canvas } from '@react-three/fiber'
import Box from '../../3D/Box/Box'
import {
  CameraControls,
  Text,
  Html,
  GizmoHelper,
  GizmoViewport,
  Line
} from '@react-three/drei'
import { format } from 'date-fns'

import s from './bar.module.scss'
import Lights from '../../3D/Lights/Lights'
import Axis from '../../3D/Axis/Axis'

function Bar({ data, axis }) {
  const cameraControlsRef = useRef()

  const minMaxValues = useMemo(() => {
    let rowsCount = data?.data?.length
    let columnsCount = 0
    let minValue = 0
    let maxValue = 0
    data?.data?.forEach?.((item) => {
      const values = Object.values(item)
      if (values.length > columnsCount) columnsCount = values.length
      values?.forEach((value) => {
        if (value > maxValue) maxValue = value
        if (value < minValue) minValue = value
      })
    })
    const difference = maxValue - minValue
    return { minValue, maxValue, rowsCount, columnsCount, difference }
  }, [data])

  const distanceBetweenBoxes = 0.01
  const maxHeight = 3

  const axisLengths = useMemo(() => ({
    x: (minMaxValues.columnsCount * 0.4 + 1) * 1.3,
    y: maxHeight * 1.3,
    z: (minMaxValues.rowsCount * 0.4 + 1) * 1.3
  }), [maxHeight, minMaxValues])

  return (
    <div className={s.root}>
      <Canvas shadows colorManagement camera={{ fov: 45, position: [25, 15, 25] }} gl={{ antialias: true }}>
        <CameraControls
          ref={cameraControlsRef}
          minDistance={5}
          // enabled={enabled}
          // verticalDragToForward={verticalDragToForward}
          // dollyToCursor={dollyToCursor}
          // infinityDolly={infinityDolly}
        />

        <Lights />

        <group>
          <Axis axis={axis} axisLengths={axisLengths} />

          {data?.data?.map?.((item, i) => {
            try {
              const date = format(new Date(item?.A), 'dd.MMM.yyyy')
              const entries = Object.entries(item)
              return (
                <Fragment key={`bar-item-${i}`}>
                  {entries?.map(([key, value], i2) => {
                    if (!i2) return
                    const height = maxHeight * value / (minMaxValues?.maxValue ?? 1)
                    const position = [(0.5 + distanceBetweenBoxes) * i2, height / 2, 0.5 + (0.5 + distanceBetweenBoxes) * i]
                    const positionOfFantom = [(0.5 + distanceBetweenBoxes) * i2, maxHeight / 2, 0.5 + (0.5 + distanceBetweenBoxes) * i]
                    const header = data?.headers[0][key]
                    return (
                      <Fragment key={`bar-item-${i}-${i2}`}>
                        <Box
                          hasEdges
                          dimensions={[0.4, height, 0.4]}
                          position={position}
                          color={'#ff9800'}
                          hoverColor={'gray'}
                          // transparent
                          // opacity={0.5}
                          label={
                            <div className={s.label}>
                              <p className={s.date}>{date}</p>
                              <p className={s.brand}>{header}</p>
                              <p className={s.value}>{value} USD</p>
                            </div>
                          }
                        />
                        {/*<Box*/}
                        {/*  dimensions={[0.5, maxHeight, 0.5]}*/}
                        {/*  position={positionOfFantom}*/}
                        {/*  color={'white'}*/}
                        {/*  hoverColor={'white'}*/}
                        {/*  transparent*/}
                        {/*  opacity={0.2}*/}
                        {/*/>*/}
                      </Fragment>
                    )
                  })}
                </Fragment>
              )
            } catch (error) {
              return null
            }
          })}
        </group>
      </Canvas>
    </div>
  )
}

export default Bar
