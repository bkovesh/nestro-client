import React from 'react'
import clsx from 'clsx'

import s from './chart-filters.module.scss'

const ChartFilters = (props) => {
  const { data: dataFromProps, onSelect, selected } = props ?? {}
  const { headers, data } = dataFromProps ?? {}

  const header = Object.entries(headers?.[1] ?? {})

  return (
    <div className={s.root}>
      <div className={s.label}>Фильтр по столбцам</div>
      <div className={s.list}>
        {header?.map?.(([key, value]) => {
          if (!value) return
          return (
            <button
              className={clsx(s.button, { [s.selected]: key === selected })}
              key={key}
              onClick={() => onSelect?.(key)}
            >
              {value}
            </button>
          )
        })}
      </div>
    </div>
  )
}

export default ChartFilters
