import React from 'react'
import {
  Html,
  Line
} from '@react-three/drei'

import s from './axis.module.scss'

function Axis(props) {
  const { axis, axisLengths } = props

  return (
    <>
      {/* X */}
      {!!axis.x &&
      <>
        <Line
          points={[[0, 0, 0], [axisLengths.x, 0, 0]]}
          color={'#000000'}
          opacity={0.5}
          lineWidth={1}
        />
        <Html
          as='div' // Wrapping element (default: 'div')
          position={[axisLengths.x + 1, -1, -1]}
          wrapperClass // The className of the wrapping element (default: undefined)
          prepend // Project content behind the canvas (default: false)
          center // Adds a -50%/-50% css transform (default: false) [ignored in transform mode]
          // fullscreen // Aligns to the upper-left corner, fills the screen (default:false) [ignored in transform mode]
          // distanceFactor={10} // If set (default: undefined), children will be scaled by this factor, and also by distance to a PerspectiveCamera / zoom by a OrthographicCamera.
          zIndexRange={[-1, 0]} // Z-order range (default=[16777271, 0])
          // transform // If true, applies matrix3d transformations (default=false)
          sprite // Renders as sprite, but only in transform mode (default=false)
        >
          <p className={s.axis}>{axis.x}</p>
        </Html>
      </>
      }
      {/* Y */}
      {!!axis.y &&
      <>
        <Line
          points={[[0, 0, 0], [0, axisLengths.y, 0]]}
          color={'#000000'}
          opacity={0.5}
          lineWidth={1}
        />
        <Html
          as='div' // Wrapping element (default: 'div')
          position={[-1, axisLengths.y + 1, -1]}
          wrapperClass // The className of the wrapping element (default: undefined)
          prepend // Project content behind the canvas (default: false)
          center // Adds a -50%/-50% css transform (default: false) [ignored in transform mode]
          // fullscreen // Aligns to the upper-left corner, fills the screen (default:false) [ignored in transform mode]
          // distanceFactor={10} // If set (default: undefined), children will be scaled by this factor, and also by distance to a PerspectiveCamera / zoom by a OrthographicCamera.
          zIndexRange={[-1, 0]} // Z-order range (default=[16777271, 0])
          // transform // If true, applies matrix3d transformations (default=false)
          sprite // Renders as sprite, but only in transform mode (default=false)
        >
          <p className={s.axis}>{axis.y}</p>
        </Html>
      </>
      }
      {/* Z */}
      {!!axis.z &&
      <>
        <Line
          points={[[0, 0, 0], [0, 0, axisLengths.z]]}
          color={'#000000'}
          opacity={0.5}
          lineWidth={1}
        />
        <Html
          as='div' // Wrapping element (default: 'div')
          position={[-1, -1, axisLengths.z + 1]}
          wrapperClass // The className of the wrapping element (default: undefined)
          prepend // Project content behind the canvas (default: false)
          center // Adds a -50%/-50% css transform (default: false) [ignored in transform mode]
          // fullscreen // Aligns to the upper-left corner, fills the screen (default:false) [ignored in transform mode]
          // distanceFactor={10} // If set (default: undefined), children will be scaled by this factor, and also by distance to a PerspectiveCamera / zoom by a OrthographicCamera.
          zIndexRange={[-1, 0]} // Z-order range (default=[16777271, 0])
          // transform // If true, applies matrix3d transformations (default=false)
          sprite // Renders as sprite, but only in transform mode (default=false)
        >
          <p className={s.axis}>{axis.z}</p>
        </Html>
      </>
      }
    </>
  )
}

export default Axis
