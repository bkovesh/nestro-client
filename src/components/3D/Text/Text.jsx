import React from 'react'
import { Text } from '@react-three/drei'

function Text({ color, data }) {
  return (
    <Text color={color} anchorX="center" anchorY="middle">
      {data}
    </Text>
  )
}

export default Text
