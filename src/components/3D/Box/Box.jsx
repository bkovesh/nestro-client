import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { Canvas, useFrame } from '@react-three/fiber'
import { Outlines, Edges, Html } from '@react-three/drei'
import * as THREE from 'three';

import s from '../../Charts/Bar/bar.module.scss'

function Box({
  opacity,
  transparent,
  position,
  dimensions,
  label,
  hasEdges = false,
  hoverColor,
  color,
  onPointerOver,
  onPointerOut
}) {
  // This reference gives us direct access to the THREE.Mesh object
  const ref = useRef()
  // Hold state for hovered and clicked events
  const [hovered, setHovered] = useState(false)
  // Subscribe this component to the render-loop, rotate the mesh every frame
  const handlePointerOver = useCallback((e) => {
    e.stopPropagation()
    setHovered(true)
    onPointerOver?.()
  }, [onPointerOver])

  const handlePointerOut = useCallback((e) => {
    e.stopPropagation()
    setHovered(false)
    onPointerOut?.()
  }, [onPointerOut])

  const htmlPosition = useMemo(() => {
    return [position[0], 1 + position[1] + dimensions[1] / 2, position[2]]
  }, [position, dimensions])
  // console.log({ dimensions, position, htmlPosition })

  return (
    <>
      <mesh
        position={position}
        ref={ref}
        scale={1}
        onPointerOver={handlePointerOver} // https://docs.pmnd.rs/react-three-fiber/api/events
        onPointerOut={handlePointerOut}
      >
        <boxGeometry args={dimensions} />
        <meshStandardMaterial
          color={hovered ? hoverColor : color}
          transparent={transparent}
          opacity={opacity}
          side={THREE.DoubleSide}
        />

        {/*<Outlines thickness={0.01} color="black" opacity={0.1} />*/}
        {hasEdges && <Edges
          scale={1.00001}
          threshold={15} // Display edges only when the angle between two faces exceeds this value (default=15 degrees)
        >
          <meshBasicMaterial transparent opacity={0.3} color='black' depthTest={true} />
        </Edges>}
      </mesh>

      {hovered && <Html
        as='div' // Wrapping element (default: 'div')
        position={htmlPosition}
        wrapperClass // The className of the wrapping element (default: undefined)
        prepend // Project content behind the canvas (default: false)
        center // Adds a -50%/-50% css transform (default: false) [ignored in transform mode]
        // fullscreen // Aligns to the upper-left corner, fills the screen (default:false) [ignored in transform mode]
        // distanceFactor={1} // If set (default: undefined), children will be scaled by this factor, and also by distance to a PerspectiveCamera / zoom by a OrthographicCamera.
        zIndexRange={[100, 0]} // Z-order range (default=[16777271, 0])
        // transform // If true, applies matrix3d transformations (default=false)
        sprite // Renders as sprite, but only in transform mode (default=false)
      >
        {label}
      </Html>
      }
    </>
  )
}

export default Box
