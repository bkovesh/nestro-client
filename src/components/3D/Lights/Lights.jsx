import React from 'react'

function Lights(props) {
  const { ambientLightIntensity = 0.7, pointLightIntensity = 20000 } = props

  return (
    <>
      <ambientLight intensity={ambientLightIntensity} />

      <pointLight intensity={pointLightIntensity} position={[-100, 100, -100]} />
      <pointLight intensity={pointLightIntensity} position={[-100, 100, 100]} />
      <pointLight intensity={pointLightIntensity} position={[100, 100, 100]} />
    </>
  )
}

export default Lights
