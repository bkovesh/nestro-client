import axios from 'axios'
import { jsonToURLSearchParams } from '../helpers/url'

export class Quotes {
  constructor() {
    this.axios = axios.create({
      baseURL: 'http://localhost:5006',
      timeout: 5000,
      headers: {}
    });
  }

  getQuotes = async (props) => {
    try {
      const search = jsonToURLSearchParams(props)
      const question = !!search ? '?' : ''
      const result = await this.axios.get(`/quotes${question}${search}`)
      return result.data
    } catch (error) {
      console.error('@CentralBank > getQuotes', error)
      return null
    }
  }
}
