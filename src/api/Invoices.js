import axios from 'axios'

export class Invoices {
  constructor() {
    this.axios = axios.create({
      baseURL: 'http://localhost:5006',
      timeout: 5000,
      headers: {}
    });
  }

  getData = async (params) => {
    try {
      const result = await this.axios.get('/invoices', { params })
      return result.data
    } catch (error) {
      console.error('@CentralBank > getData', error)
      return null
    }
  }

  getWorkbook = async (params) => {
    try {
      const result = await this.axios.get('/invoices/workbook', { params })
      return result.data
    } catch (error) {
      console.error('@CentralBank > getWorkbook', error)
      return null
    }
  }
}
