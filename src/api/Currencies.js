import axios from 'axios'

import { jsonToURLSearchParams } from '../helpers/url'

export class Currencies {
  constructor() {
    this.axios = axios.create({
      baseURL: 'http://localhost:5006/currencies',
      timeout: 5000,
      headers: {}
    });
  }

  getCurrencyByDate = async (props) => {
    try {
      const search = jsonToURLSearchParams(props)
      const question = !!search ? '?' : ''
      const result = await this.axios.get(`/currency-by-date${question}${search}`)
      return result.data
    } catch (error) {
      console.error('@Currencies > getCurrencyByDate', error)
      return null
    }
  }

  getCurrenciesByDate = async (props) => {
    try {
      const search = jsonToURLSearchParams(props)
      const question = !!search ? '?' : ''
      const result = await this.axios.get(`/currencies-by-date${question}${search}`)
      return result.data
    } catch (error) {
      console.error('@Currencies > getCurrenciesByDate', error)
      return null
    }
  }
}