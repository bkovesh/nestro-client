export const arrayToObject = (array) => {
  let result = {}
  array.forEach(([key, value]) => {
    if (!result[key]) result[key] = {}
    result[key] = value
  })
  return result
}
