export const toFixed = (value, fractionDigits) => {
  if (typeof value === 'number') return value.toFixed(fractionDigits)
  try {
    return +value.toFixed(fractionDigits)
  } catch (error) {
    console.error(error)
    return value
  }
}
