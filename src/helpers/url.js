export const jsonToURLSearchParams = (json) => {
  const params = new URLSearchParams("a=apple&b=balloon");
  for (let key in json) params.append(key, json[key]);
  return params
}