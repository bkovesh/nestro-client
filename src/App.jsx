import { useCallback, useEffect, useState } from 'react'
import clsx from 'clsx';
import { BigNumber as BN } from 'bignumber.js'
import { format } from 'date-fns'
import { useDebounce } from "@uidotdev/usehooks";

import s from './app.module.scss'

import Logo from './assets/logo.svg?react'

import { Invoices as InvoicesService } from './api/Invoices'
import { Quotes as QuotesService } from './api/Quotes'
import Table from './components/Table/Table'
import Bar from './components/Charts/Bar/Bar'
import { LOCALSTORAGE_MONTH, LOCALSTORAGE_DATE } from './constants/localStorage'
import { useCalculateInvoices } from './hooks/useCalculateInvoices'
import Spreadsheet from './components/Spreadsheet/Spreadsheet'
import { useCurrencies } from './contexts/Currencies/CurrenciesProvider'
import ChartFilters from './components/ChartFilters/ChartFilters'
import InvoicesBar from './components/Charts/InvoicesBar/InvoicesBar'

const Invoices = new InvoicesService()
const Quotes = new QuotesService()

function App() {
  // forms

  const dateFromLocalStorage = localStorage.getItem(LOCALSTORAGE_DATE)
  const defaultDate = format(
    new Date(dateFromLocalStorage ?? Date.now()),
    'yyyy/MM/dd'
  )
  const [date, setDate] = useState(defaultDate)
  const [month, setMonth] = useState(localStorage.getItem(LOCALSTORAGE_MONTH))
  const debouncedMonth = useDebounce(month, 500);
  const debouncedDate = useDebounce(date, 500);

  const handleChangeDate = useCallback((e) => {
    const { value } = e.target
    localStorage.setItem(LOCALSTORAGE_DATE, value)
    setDate(value)
  }, [])

  const handleChangeMonth = useCallback((e) => {
    const { value } = e.target
    localStorage.setItem(LOCALSTORAGE_MONTH, value)
    setMonth(value)
  }, [])

  // currencies

  const { getCurrencyByDate, currencies, getCurrencies } = useCurrencies()

  useEffect(() => {
    getCurrencies(debouncedDate)
  }, [debouncedDate])

  useEffect(() => {
    if (!debouncedDate) return
    getCurrencyByDate({ date: debouncedDate, currency: 'USD' })
  }, [debouncedDate])

  // quotes

  const [quotes, setQuotes] = useState([])

  const getQuotes = useCallback(async () => {
    const result = await Quotes.getQuotes({
      sheet: debouncedMonth,
      range: 'A5:AI25',
      headers_range: 'A3:AI3',
    })
    console.log('@App > getQuotes', { result })
    setQuotes(result)
  }, [debouncedMonth])

  useEffect(() => {
    getQuotes()
  }, [debouncedMonth])

  // invoices

  const [invoices, setInvoices] = useState()
  const [invoicesRaw, setInvoicesRaw] = useState()

  const getInvoices = useCallback(async () => {
    const result = await Invoices.getData({
      sheet: 'Анализ_БК+ББ',
      header: 'A',
      headers_range: 'A1:DM2',
      range: 'A4:DM5'
    })
    setInvoices({ ...result })
    console.log('@App > getInvoices', { result })
    const resultRaw = await Invoices.getWorkbook()
    setInvoicesRaw(resultRaw)
  }, [])

  useEffect(() => {
    getInvoices()
  }, [])

  const data = useCalculateInvoices({ invoices })

  const [selectedColumn, setSelectedColumn] = useState('A')

  const handleSelectColumn = useCallback((data) => {
    setSelectedColumn(data)
  }, [])

  return (
    <div className={s.root}>
      <header className={s.header}>
        <div className={s.logoContainer}>
          <Logo className={s.logo} />
        </div>
      </header>

      <div className={s.currencies}>
        <h2>Курсы валют от ЦБ</h2>
        <div className={s.date}>
          <div className={s.label}>Дата:</div>
          <input className={s.input} type="text" value={date} onChange={handleChangeDate}/>
        </div>
        <div className={s.list}>
          {currencies?.map(([key, value], i) => {
            const direction = value?.Value === value?.Previous ?
              'neutral' :
              value?.Value > value?.Previous ? 'up' : 'down'
            const difference = new BN(value?.Value).minus(value?.Previous).toString()
            return (
              <div className={clsx(s.item, { [s[direction]]: !!direction })} key={`currency-${i}`}>
                <div className={s.label}>{key}:</div>
                <div className={s.valueContainer}>
                  <div className={s.value}>{value?.Value}</div>
                  <div className={s.difference}>({difference})</div>
                </div>
              </div>
            )
          })}
        </div>
      </div>

      <div className={s.charts}>
        <h2>Динамика цены барреля нефти в течение месяца</h2>
        <div className={s.label}>Данные из Приложения 2.</div>
        <div className={s.month}>
          <div className={s.label}>Месяц:</div>
          <input className={s.input} type="text" value={month} onChange={handleChangeMonth}/>
        </div>
        <div className={s.chart}>
          <Bar axis={{ x: 'Бренды', y: 'Цена, USD/барр', z: 'Даты' }} data={quotes} />
        </div>
      </div>

      <div>
        <h2>Сводная таблица интерактивная</h2>
        <div className={s.label}>Данные из Приложения 1 + Произведены некоторые предварительные расчеты</div>
        <div className={s.spreadsheetContainer}>
          <Spreadsheet data={invoicesRaw} />
        </div>
      </div>

      <div className={s.charts}>
        <h2>Данные сводной таблицы на графике</h2>
        <ChartFilters onSelect={handleSelectColumn} selected={selectedColumn} data={invoices} />
        <div className={s.chart}>
          <InvoicesBar
            axis={{ z: 'Строки таблицы', y: 'Значение' }}
            data={invoices}
            selectedColumn={selectedColumn}
          />
        </div>
      </div>

      {/*<div className={s.tableContainer}>*/}
      {/*  <h2>Сводная таблица с расчетами</h2>*/}
      {/*  <div className={s.label}>Данные из Приложения 1</div>*/}
      {/*  <Table headers={invoices?.headers} data={data} />*/}
      {/*</div>*/}
    </div>
  )
}

export default App
